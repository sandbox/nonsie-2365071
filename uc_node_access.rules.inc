<?php

/**
 * @file
 * Rules hooks and functions for uc_node_access.module.
 */

/**
 * Implements hook_rules_data_info().
 */
function uc_node_access_rules_data_info() {
  $data['uc_node_access_products'] = array(
    'label' => t('Ubercart node access'),
    'wrap' => TRUE,
    'group' => t('Node Access'),
    'token type' => 'uc_node_access',
  );

  return $data;
}

/**
 * Implements hook_rules_action_info().
 * Define rules compatible actions.
 */
function uc_node_access_rules_action_info() {
  $actions['uc_node_access_delay_access'] = array(
    'label' => t('Grant node access to customer at a later time'),
    'group' => t('UC Node Access'),
    'parameter' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order')),
    ),
  );

  $actions['uc_node_access_grant_access'] = array(
    'label' => t('Grant node access to customer'),
    'group' => t('UC Node Access'),
    'parameter' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order')),
    ),
  );

  return $actions;
}

/**
 * Implements hook_rules_event_info().
 * Define rules events.
 */
function uc_node_access_rules_event_info() {
  $events['uc_node_access_grant'] = array(
    'label' => t('Node access is granted to a user'),
    'group' => t('Node access'),
    'base'  => 'uc_node_access_grant_access',
    'variables' => array(
      'node' => array(
        '#entity' => 'node',
        '#title' => t('Node'),
      ),
      'user' => array(
        '#entity' => 'user',
        '#title' => t('User'),
      ),
    ),
  );
  $events['uc_node_access_revoke'] = array(
    'label' => t('Node access is revoked from a user'),
    'group' => t('Node access'),
    'base'  => 'uc_node_access_revoke_access',
    'variables' => array(
      'node' => array(
        '#entity' => 'node',
        '#title' => t('Node'),
      ),
      'user' => array(
        '#entity' => 'user',
        '#title' => t('User'),
      ),
    ),
  );

  return $events;
}

function uc_node_access_delay_access($order) {
  if (!$order->uid || !($order_user = user_load($order->uid))) {
    return;
  }

  if (is_array($order->products)) {
    $nids = array();
    $models = array();

    foreach ($order->products as $product) {
      $nids[] = $product->nid;
      $models[] = $product->model;
    }

    $query = db_select('uc_product_features', 'pf');
    $query->fields('pf', array('pfid'));
    $query->leftJoin('uc_node_access_products', 'nap', 'pf.pfid = nap.pfid');
    $query->fields('nap', array('model'));
    $query->condition('pf.nid', $nids, 'IN');
    $query->condition('pf.fid', 'node_access', '=');
    $result = $query->execute();

    foreach ($result as $row) {
      if (empty($row->model) || in_array($row->model, $models)) {
        uc_node_access_delay_user_access($row->pfid, $order->uid);
      }
    }
  }
}

function uc_node_access_grant_access($order) {
  if (is_array($order->products)) {
    $nids = array();
    $models = array();

    foreach ($order->products as $product) {
      $nids[] = $product->nid;
      $models[] = $product->model;
    }

    $query = db_select('uc_product_features', 'pf');
    $query->fields('pf', array('pfid'));
    $query->leftJoin('uc_node_access_products', 'nap', 'pf.pfid = nap.pfid');
    $query->fields('nap', array('model'));
    $query->condition('pf.nid', $nids, 'IN');
    $query->condition('pf.fid', 'node_access', '=');
    $result = $query->execute();

    foreach ($result as $row) {
      if (empty($row->model) || in_array($row->model, $models)) {
        uc_node_access_grant_user_access($row->pfid, $order->uid);
      }
    }
  }
}