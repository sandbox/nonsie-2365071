<?php

/**
 * @file
 * Default Rules configurations for uc_node_access.module.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uc_node_access_default_rules_configuration() {
  // Grant node access on order update.
  $rule = rules_reaction_rule();
  $rule->label = t('Grant appropriate node access on ORDER UPDATE');
  $rule->active = TRUE;
  $rule->event('uc_order_status_update');
  $rule->condition(rules_condition('data_is', array('data:select' => 'order:order-status', 'value' => 'completed'))->negate());
  $rule->condition(rules_condition('data_is', array('data:select' => 'updated_order:order-status', 'value' => 'completed')));
  $rule->action('uc_node_access_grant_access', array('order:select' => 'updated_order'));
  $configs['uc_node_access_grant_on_completed_status'] = $rule;

  return $configs;
}